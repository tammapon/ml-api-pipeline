FROM ultralytics/yolov5:v7.0-cpu

WORKDIR /app

RUN apt update && apt install -y libgl1

COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["uvicorn", "infer:app", "--host", "0.0.0.0", "--port", "80", "--reload"]
