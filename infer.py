from typing import List
import torch
from fastapi import FastAPI, UploadFile, File
import numpy as np
import cv2

app = FastAPI()


@app.on_event("startup")
async def startup_event():
    global MODEL
    MODEL = torch.hub.load('ultralytics/yolov5', 'yolov5s')


@app.post("/predict")
async def predict(file: UploadFile):

    content_batch = await file.read()
    cv2_img = cv2.imdecode(np.frombuffer(content_batch, np.uint8), cv2.IMREAD_COLOR)
        
    results = MODEL(cv2_img)

    return {
        "data": results.pandas().xyxy[0].to_dict()
    }


# @app.post("/predict")
# async def predict(files: List[UploadFile] = File(...)):

#     contents = []
#     for file_batch in files:
#         content_batch = await file_batch.read()
#         cv2_img = cv2.imdecode(np.frombuffer(content_batch, np.uint8), cv2.IMREAD_COLOR)
#         contents.append(cv2_img)

#     results = MODEL(contents)

#     data = []
#     for i in range(len(files)):
#         result = results.pandas().xyxy[i].to_dict()
#         data.append(result)

#     return {
#         "data": data
#     }
